// ==UserScript==
// @name         WoB Tool
// @namespace    https://worldofbleach-rpg.com/
// @version		 0.3.3.0
// @description  Does stuff in WoB
// @include	 	 *worldofbleach-rpg.com/*
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_openInTab
// @grant        GM_listValues
// @updateURL    https://bitbucket.org/Tina_azure/wob-helper/raw/master/main.user.js
// @downloadURL  https://bitbucket.org/Tina_azure/wob-helper/raw/master/main.user.js
// ==/UserScript==

//General Information
var defaultCaptchaOld					= localStorage.getItem('defaultCaptchaOld');
var characterName						= localStorage.getItem('characterName');
var firstRun 							= localStorage.getItem(characterName + 'firstRun');
var specialization 						= localStorage.getItem(characterName + 'specialization');
var rank 								= localStorage.getItem(characterName + 'rank');
var squad 								= localStorage.getItem(characterName + 'squad');
//Daily	
var dateForNewDaily 					= localStorage.getItem(characterName + 'dateForNewDaily');
var dailyTask 							= localStorage.getItem(characterName + 'dailyTask');
var dailyFights 						= localStorage.getItem(characterName + 'dailyFights');
//Change Pool Position	
var battlePoolReplacement  				= localStorage.getItem(characterName + 'battlePoolReplacement');
//DamageSummation	
var DamageSummationLastBoss  		    = localStorage.getItem(characterName + 'DamageSummationLastBoss');
//Instant Messanger	
var instantMessageNotificationCheck 	= localStorage.getItem(characterName + 'instantMessageNotificationCheck');
var instantMessageNotificationConf  	= localStorage.getItem(characterName + 'instantMessageNotificationConf');
var instantMessageNotificationBlock 	= localStorage.getItem(characterName + 'instantMessageNotificationBlock');
var instantMessageNotificationException = false;


(function()
{
	if (/.*worldofbleach-rpg.com\/\?character_select=1/.exec(window.location.href) != null)
	{
		selectCharacter();
	}
	else
	{
		if(firstRun != 1)
		{
			init();
		}
		if(/.*worldofbleach-rpg.com\/\?id=21/.exec(window.location.href) != null)
		{
			createConfig();
		}
		if(/.*worldofbleach-rpg.com\/\?id=15/.exec(window.location.href) != null)
		{
            activeUsersCentralHQ();
		}
		if(/.*worldofbleach-rpg.com\/\?id=10&battle_type=.*/.exec(window.location.href) != null)
		{
            window.setInterval(showDailyArenaFights, 1000);
		}
        if(instantMessageNotificationCheck)
        {
            let imncInterval = 5000;
            if(instantMessageNotificationConf & 4)
            {
                imncInterval = 15000;
            }
            window.setInterval(instantMessageNotification, imncInterval);
        }
		if(document.getElementById("battle") != null)
		{
			if(battlePoolReplacement == "true")
			{
				movePoolsBattle();
			}
			window.setInterval(damageSummation, 500);
		}
        if(defaultCaptchaOld  == "true" && typeof(captcha) != "undefined")
        {
        	if(captcha.style.display != 'none')
        	{
        		captcha.style.display = 'none';
  				altCaptcha.style.display = 'inline-block';
        	}
        }
	}
})();

function damageSummation()
{
	let DamageSummationCurrentBattleDone    = localStorage.getItem(characterName + 'DamageSummationCurrentBattleDone');
	let DamageSummationCurrentBattleTaken   = localStorage.getItem(characterName + 'DamageSummationCurrentBattleTaken');
	let DamageSummationCurrentBattlePrevPD  = localStorage.getItem(characterName + 'DamageSummationCurrentBattlePrevPD');
	let DamageSummationCurrentBattlePrevOD  = localStorage.getItem(characterName + 'DamageSummationCurrentBattlePrevOD');
	let elePD = document.getElementsByClassName("playerDamage");
	let eleOD = document.getElementsByClassName("opponentDamage");
	let regex = /[A-z| ]*(\d*\.\d*) (.*)/;	
	let finished = false;
	
	if(elePD.length != 0 && !finished)
	{
		elePD = elePD[0].textContent;
		if(DamageSummationCurrentBattlePrevPD != elePD && elePD.includes("damage"))
		{
			if(isNaN(parseFloat(DamageSummationCurrentBattleDone)))
			{
				DamageSummationCurrentBattleDone = "0.0";
			}
			let damage = parseFloat(regex.exec(elePD)[1]);
			damage += parseFloat(DamageSummationCurrentBattleDone);
			DamageSummationCurrentBattlePrevPD = elePD;
			localStorage.setItem(characterName + 'DamageSummationCurrentBattleDone', damage);
			localStorage.setItem(characterName + 'DamageSummationCurrentBattlePrevPD', DamageSummationCurrentBattlePrevPD);
		}
	}
	if(eleOD.length != 0 && !finished)
	{
		eleOD = eleOD[0].textContent;
		if(DamageSummationCurrentBattlePrevOD != eleOD && eleOD.includes("damage"))
		{
			if(isNaN(parseFloat(DamageSummationCurrentBattleTaken)))
			{
				DamageSummationCurrentBattleTaken = "0.0";
			}
			let damage = parseFloat(regex.exec(eleOD)[1]);
			damage += parseFloat(DamageSummationCurrentBattleTaken); 
			DamageSummationCurrentBattlePrevOD = eleOD;
			localStorage.setItem(characterName + 'DamageSummationCurrentBattleTaken', damage);
			localStorage.setItem(characterName + 'DamageSummationCurrentBattlePrevOD', DamageSummationCurrentBattlePrevOD);
		}
	}
	let battleElement = document.getElementById("battle").childNodes[0].childNodes[1].childNodes[0].childNodes[4];
	if(battleElement != undefined && (battleElement.textContent.includes("You win!") || battleElement.textContent.includes("knocks you out")))
	{

	    DamageSummationCurrentBattleDone    = localStorage.getItem(characterName + 'DamageSummationCurrentBattleDone');
	    DamageSummationCurrentBattleTaken   = localStorage.getItem(characterName + 'DamageSummationCurrentBattleTaken');
	    let dSCBD = parseFloat(DamageSummationCurrentBattleDone);
	    let dSCBT = parseFloat(DamageSummationCurrentBattleTaken);
	    if(isNaN(dSCBD)) { dSCBD = 0.0; }
	    if(isNaN(dSCBT)) { dSCBT = 0.0; }
		if(!battleElement.innerHTML.slice(0,-5).includes("The Enemy has taken "))
		{
			let bEiHTML = battleElement.innerHTML.slice(0,-5);
			let dmgDone = "<br>The Enemy has taken " + dSCBD.toFixed(2) + " damage.";
			let dmgTaken = "<br>The Enemy has done " + dSCBT.toFixed(2) + " damage.";
			battleElement.innerHTML = bEiHTML + dmgDone + dmgTaken + "</td>";
		}
		localStorage.setItem(characterName + 'DamageSummationCurrentBattleDone', "");
		localStorage.setItem(characterName + 'DamageSummationCurrentBattlePrevPD', "");
		localStorage.setItem(characterName + 'DamageSummationCurrentBattleTaken', "");
		localStorage.setItem(characterName + 'DamageSummationCurrentBattlePrevOD', "");
		finished = true;
	}
	
}

function movePoolsBattle()
{
	let x = document.getElementById("battle").childNodes[0].childNodes[1].childNodes[0];
	let battlePoolNode = x.childNodes[1];
    let lastBattlePoolNode = x.childNodes[x.childElementCount-1];
	x.appendChild(battlePoolNode);
	x.appendChild(lastBattlePoolNode);
}

function showDailyArenaFights()
{
	let battleElement = document.getElementById("battle").childNodes[0].childNodes[1].childNodes[0].childNodes[4];
	if(battleElement != undefined && battleElement.textContent.includes("You win!"))
	{
		if(!battleElement.innerHTML.slice(0,-5).includes("Arena Fights for your Daily Task."))
		{
			let connection = new XMLHttpRequest();
    		connection.open("POST", "https://worldofbleach-rpg.com/react.php?page=barracks", true);
    		connection.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			connection.send("room=warRoom");
			setTimeout(function()
			{
				let dailyElement = JSON.parse(connection.responseText);
				if(dailyElement.dailyTask.label.includes("Fight Arena battles") && (dailyElement.dailyTaskProgress != -1))
				{
					let x = dailyElement.dailyTaskProgress;
					let y = dailyElement.dailyTask.amount;
					let bEiHTML = battleElement.innerHTML.slice(0,-5);
					battleElement.innerHTML = bEiHTML + "<br>You have completed " + x + "/" + y + " Arena Fights for your Daily Task." + "</td>";
				}
			}, 500);
		}
	}

}

function activeUsersCentralHQ()
{
	let shiniActiveInactive = [0,0];
	let xenosActiveInactive = [0,0];
	let shiniActiveInactiveRanks = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	let xenosActiveInactiveRanks = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];

	let connection = new XMLHttpRequest();
    connection.open("GET", "https://worldofbleach-rpg.com/react.php?page=onlineUsers", false);
    connection.send(null);
	let listActiveInactive = connection.response.substring(21).split("},");

	let activeCheck = 0;
	for(i = 0; i < listActiveInactive.length; i++)
	{
		if(listActiveInactive[i].includes("Inactive Users"))
		{
			activeCheck = 7;
		}
		if(listActiveInactive[i].includes("Shinigami"))
		{
				 if (listActiveInactive[i].includes("Student")) 			{ shiniActiveInactiveRanks[activeCheck + 0] += 1; }
			else if (listActiveInactive[i].includes("Squad Member")) 		{ shiniActiveInactiveRanks[activeCheck + 1] += 1; }
			else if (listActiveInactive[i].includes("Senior Squad Member")) { shiniActiveInactiveRanks[activeCheck + 2] += 1; }
			else if (listActiveInactive[i].includes("Elite Officer")) 		{ shiniActiveInactiveRanks[activeCheck + 3] += 1; }
			else if (listActiveInactive[i].includes("Elite Officer")) 		{ shiniActiveInactiveRanks[activeCheck + 4] += 1; }
			else if (listActiveInactive[i].includes("Veteran Shinigami")) 	{ shiniActiveInactiveRanks[activeCheck + 5] += 1; }
			else if (listActiveInactive[i].includes("Legendary Shinigami")) { shiniActiveInactiveRanks[activeCheck + 6] += 1; }
			else 															{ shiniActiveInactiveRanks[activeCheck + 6] += 1; }
				 if (activeCheck === 0) 									{ shiniActiveInactive[0] += 1; }
			else 															{ shiniActiveInactive[1] += 1; }
		}
		if(listActiveInactive[i].includes("Hollow"))
		{
				 if (listActiveInactive[i].includes("Devouring Beast")) 	{ xenosActiveInactiveRanks[activeCheck + 0] += 1; }
			else if (listActiveInactive[i].includes("Huge Hollow")) 		{ xenosActiveInactiveRanks[activeCheck + 1] += 1; }
			else if (listActiveInactive[i].includes("Gillian")) 			{ xenosActiveInactiveRanks[activeCheck + 2] += 1; }
			else if (listActiveInactive[i].includes("Arrancar")) 			{ xenosActiveInactiveRanks[activeCheck + 3] += 1; }
			else if (listActiveInactive[i].includes("Elite Fraccion")) 		{ xenosActiveInactiveRanks[activeCheck + 4] += 1; }
			else if (listActiveInactive[i].includes("Proximo Espada")) 		{ xenosActiveInactiveRanks[activeCheck + 5] += 1; }
			else if (listActiveInactive[i].includes("Vasto Lorde"))			{ xenosActiveInactiveRanks[activeCheck + 6] += 1; }
			else 															{ xenosActiveInactiveRanks[activeCheck + 6] += 1; }
				 if (activeCheck === 0) 									{ xenosActiveInactive[0] += 1; }
			else 															{ xenosActiveInactive[1] += 1; }
		}
	}

	let centralHQElement = document.getElementById('pageContent').childNodes[document.getElementById('pageContent').childNodes.length-1].childNodes[1].childNodes[4];
	let shinEle = centralHQElement.childNodes[1].childNodes;
	let xenoEle = centralHQElement.childNodes[3].childNodes;
	for(i = 8, j = 0; i < shinEle.length; i += 4, j++)
	{
		shinEle[i].textContent += "\t-  " + shiniActiveInactiveRanks[j] + "/" + shiniActiveInactiveRanks[7 + j];
		xenoEle[i].textContent += "\t-  " + xenosActiveInactiveRanks[j] + "/" + xenosActiveInactiveRanks[7 + j];
	}
	let sL = centralHQElement.childNodes[1].childNodes.length;
	let xL = centralHQElement.childNodes[3].childNodes.length;
	let copynodesShini = [shinEle[sL-4].cloneNode(true), shinEle[sL-3].cloneNode(true), shinEle[sL-2].cloneNode(true), shinEle[sL-1].cloneNode(true)];
	let copynodesXenos = [xenoEle[xL-4].cloneNode(true), xenoEle[xL-3].cloneNode(true), xenoEle[xL-2].cloneNode(true), xenoEle[xL-1].cloneNode(true)];
	copynodesShini[0].textContent = "Online - Active/Inactive:";
	copynodesXenos[0].textContent = "Online - Active/Inactive:";
	copynodesShini[1].textContent = "\t\t" + (shiniActiveInactive[0] + shiniActiveInactive[1]) + "\t-  " + shiniActiveInactive[0] + "/" + shiniActiveInactive[1];
	copynodesXenos[1].textContent = "\t\t" + (xenosActiveInactive[0] + xenosActiveInactive[1]) + "\t-  " + xenosActiveInactive[0] + "/" + xenosActiveInactive[1];
	for(i = 0; i < 4; i++)
	{
		centralHQElement.childNodes[1].appendChild(copynodesShini[i]);
		centralHQElement.childNodes[3].appendChild(copynodesXenos[i]);
	}
}

function createConfig()
{
	let menuElement = document.getElementById('pageContent');
	for (let i = menuElement.childNodes[11].childNodes[1].children.length - 1; i >= 0; i--)
	{
		if(menuElement.childNodes[11].childNodes[1].children[i].textContent.includes("Misc Settings"))
		{
			let childnodes = menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].childNodes;
			let childrenToAppendToEnd = [];
			let childrenToAppendToMid = [childnodes[0].cloneNode(true), childnodes[1].cloneNode(true)];
			for (let j = childnodes.length - 1; j >= 0; j--) {
				if (childnodes[j].className == "subtle")
				{
					childrenToAppendToEnd[0] = childnodes[j-1].cloneNode(true);
					childrenToAppendToEnd[1] = childnodes[j  ].cloneNode(true);
					childrenToAppendToEnd[2] = childnodes[j+1].cloneNode(true);
					childrenToAppendToEnd[3] = childnodes[j+2].cloneNode(true);
					break;
				}
			}
			//Seperator Line
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToEnd[0].cloneNode(true));
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToEnd[1].cloneNode(true));
			//Config Elements
			let NodeIMNotification = childrenToAppendToMid[1].cloneNode(true);
			NodeIMNotification.childNodes[1].innerText = "Messenger Notifications";
			NodeIMNotification.childNodes[4].name = "IM_Notifications";
			NodeIMNotification.childNodes[4].style.width = "150px";
			NodeIMNotification.childNodes[4][0].innerText = "No Notifications";
			NodeIMNotification.childNodes[4][1].innerText = "Beep Notification";
			let opt2 = document.createElement("option");
			opt2.value = 2;
			opt2.text = "Alert Notification";
			NodeIMNotification.childNodes[4].add(opt2);
			let opt3 = document.createElement("option");
			opt3.value = 4;
			opt3.text = "Push Notification";
			NodeIMNotification.childNodes[4].add(opt3);
			let opt4 = document.createElement("option");
			opt4.value = 5;
			opt4.text = "Push+beep Notification";
			NodeIMNotification.childNodes[4].add(opt4);
			if(instantMessageNotificationConf == 0)
			{
				NodeIMNotification.childNodes[4].options[0].selected = true;
			}
			else if(instantMessageNotificationConf == 1)
			{
				NodeIMNotification.childNodes[4].options[1].selected = true;
			}
			else if(instantMessageNotificationConf == 2)
			{
				NodeIMNotification.childNodes[4].options[2].selected = true;
			}
			else if(instantMessageNotificationConf == 4)
			{
				NodeIMNotification.childNodes[4].options[3].selected = true;
			}
			else if(instantMessageNotificationConf == 5)
			{
				NodeIMNotification.childNodes[4].options[4].selected = true;
			}
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToMid[0].cloneNode(true));
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(NodeIMNotification);

			//Battle Pool Replacement CB
			let bPRcheckbox = document.createElement('input');
			bPRcheckbox.type = "checkbox";
			bPRcheckbox.name = "Battle Pool Replacement";
			bPRcheckbox.value = "Battle Pool Replacement";
			bPRcheckbox.id = "battlePoolReplacementCB";
            if(battlePoolReplacement == null) {battlePoolReplacement = "false";}
            if(battlePoolReplacement === "true") { bPRcheckbox.checked = true; }
            else { bPRcheckbox.checked = false; }
    		let bPRLabelElement = document.createElement("LABEL");
    		let bPRLabelElementP2 = document.createTextNode("Battle Pool Replacement: ");
    		bPRLabelElement.appendChild(bPRLabelElementP2);
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(bPRLabelElement);
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(bPRcheckbox);

			//Set Alt Captcha as default
			let sACcheckbox = document.createElement('input');
			sACcheckbox.type = "checkbox";
			sACcheckbox.name = "Set alt Captcha as Default";
			sACcheckbox.value = "Set alt Captcha as Default";
			sACcheckbox.id = "altCaptchaSetAsDefault";
            if(defaultCaptchaOld == null) {defaultCaptchaOld = "false";}
            if(defaultCaptchaOld === "true") { sACcheckbox.checked = true; }
            else { sACcheckbox.checked = false; }
    		let sACLabelElement = document.createElement("LABEL");
    		let sACLabelElementP2 = document.createTextNode(" | Set alt Captcha as Default: ");
    		sACLabelElement.appendChild(sACLabelElementP2);
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(sACLabelElement);
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(sACcheckbox);

			//Config Save Button
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToEnd[0].cloneNode(true));
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToEnd[1].cloneNode(true));
			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(childrenToAppendToEnd[2].cloneNode(true));
			let btnNode = childrenToAppendToEnd[3].cloneNode(true);
			btnNode.value = "Submit WoB-Helper Config";
            btnNode.style.width = "187px";
			btnNode.formAction = null;
			btnNode.type = null;
			btnNode.onclick = function()
			{
				let NodeIMNotification = menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1][6];
				if(NodeIMNotification.options[0].selected)
				{
					instantMessageNotificationCheck = true;
					instantMessageNotificationConf = 0;
				}
				else
				{
					instantMessageNotificationCheck = false;
					if (NodeIMNotification.options[1].selected == true)
					{
						instantMessageNotificationConf = 1;
					}
					else if(NodeIMNotification.options[2].selected == true)
						 {
						 	instantMessageNotificationConf = 2;
					     }
						 else if(NodeIMNotification.options[3].selected == true)
                              {
                              	if (Notification.permission !== "granted")
                              	{
    								Notification.requestPermission();
                              	}
                              	if(Notification.permission == "granted")
                              	{
                              	  instantMessageNotificationConf = 4;
                              	}
                			  }
                			  else
                			  {
                              	if (Notification.permission !== "granted")
                              	{
    								Notification.requestPermission();
                              	}
                              	if (Notification.permission == "granted")
                              	{
                              	  instantMessageNotificationConf = 5;
                              	}
                			  }
				}
				if (bPRcheckbox.checked)
				{
					battlePoolReplacement = true;
				}
				else
				{
					battlePoolReplacement = false;
				}
				if (sACcheckbox.checked)
				{
					defaultCaptchaOld = true;
				}
				else
				{
					defaultCaptchaOld = false;
				}
				localStorage.setItem('defaultCaptchaOld',defaultCaptchaOld);
				localStorage.setItem(characterName + 'battlePoolReplacement',battlePoolReplacement);
				localStorage.setItem(characterName + 'instantMessageNotificationCheck',instantMessageNotificationCheck);
				localStorage.setItem(characterName + 'instantMessageNotificationConf',instantMessageNotificationConf);
			};

			menuElement.childNodes[11].childNodes[1].children[i+1].childNodes[1].childNodes[1].appendChild(btnNode);
		}

	}
}

function selectCharacter()
{
	let charSelElement = document.getElementById('characterSelect');
	for (let i = 0; i < charSelElement.childNodes.length; i++)
	{
		if (charSelElement.childNodes[i].className == "characterDisplay")
		{
			let charSelName = charSelElement.childNodes[i].children[1].firstChild;
			charSelElement.childNodes[i].children[3].click = function()
			{
				localStorage.setItem('characterName', charSelName);
				characterName = charSelName;
			};
		}
	}
}

function init()
{
	if (/.*worldofbleach-rpg.com\/\?id=1$/.exec(window.location.href) != null)
	{
		firstRun = 1;
		localStorage.setItem(characterName + 'firstRun', 1);
		initSpecialization();
		initRank();
		initSquad();
	}
	else
	{
		window.location.assign('https://worldofbleach-rpg.com/?id=1');
	}
}

function initSquad()
{
	squad = '0';
	let squadElement = document.getElementById('characterInfoContainer');
	for (let i = 0; i < squadElement.childNodes[1].childNodes.length; i++)
	{
		if(squadElement.childNodes[1].childNodes[i].textContent.includes("Elite Squad:"))
		{
			i++;
			squad = squadElement.childNodes[1].childNodes[i].textContent.trim();
			break;
		}
	}
	localStorage.setItem(characterName + 'squad', squad);
}

function initSpecialization()
{
	let specElement = document.getElementById('statInfoContainer');
	specialization = ['-','-'];
	for (let i = 0; i < specElement.childNodes.length; i++)
	{
		if(specElement.childNodes[i].textContent.includes("Specialization"))
		{
			i += 2;
			if(!specElement.childNodes[i].innerText.includes('none'))
			{
				specialization = specElement.childNodes[i].innerText.slice(15).split(' - ');
			}
			break;
		}
	}
	localStorage.setItem(characterName + 'specialization', specialization);
}

function initRank()
{
	let rankElement = document.getElementById('primaryInfoContainer');
	for (let i = 0; i < rankElement.childNodes.length; i++)
	{
		if(rankElement.childNodes[i].textContent.includes("Experience:"))
		{
			let tmpRank = rankElement.childNodes[i].innerText.split(' / ')[1];
			switch(tmpRank)
			{
				case '140,000':
					rank = 1;
					break;
				case '270,000':
					rank = 2;
					break;
				case '1,100,000':
					rank = 3;
					break;
				case '3,300,000':
					rank = 4;
					break;
				case '13,000,000':
					rank = 5;
					break;
				case '45,000,000':
					rank = 6;
					break;
				case '150,000,000':
					rank = 7;
					break;
				case '450,000,000':
					rank = 8;
					break;
				default:
					rank = 1;
					break;
			}
			break;
		}
	}
	localStorage.setItem(characterName + 'rank', rank);
}

function instantMessageNotification()
{
	let newMessage = false;
	let newMessagesFrom = "";
	let messageElement = document.getElementById('messages');
	for(let i = 0; i < messageElement.childNodes[0].childNodes[0].childNodes[1].firstChild.childNodes.length; i++)
	{
		if(messageElement.childNodes[0].childNodes[0].childNodes[1].firstChild.childNodes[i].lastChild.className == "menuAlert")
	    {
            instantMessageNotificationBlock = localStorage.getItem(characterName + 'instantMessageNotificationBlock');
            if(instantMessageNotificationBlock != 1)
            {
                instantMessageNotificationBlock = 1;
                localStorage.setItem(characterName + 'instantMessageNotificationBlock', instantMessageNotificationBlock);
                instantMessageNotificationException = true;
            }
			newMessage = true;
		    newMessagesFrom += messageElement.childNodes[0].childNodes[0].childNodes[1].firstChild.childNodes[i].innerText.slice(0,-1) + '\n';
	    }
	}
	newMessagesFrom = newMessagesFrom.slice(0,-1);
	if(newMessage)
	{
		if(instantMessageNotificationConf & 1)
		{
            if(instantMessageNotificationBlock == 1 && instantMessageNotificationException == true)
            {
                beep();
                instantMessageNotificationBlock = 0;
                localStorage.setItem(characterName + 'instantMessageNotificationBlock', instantMessageNotificationBlock);
                instantMessageNotificationException = false;
            }
		}
		if(instantMessageNotificationConf & 2)
		{
			window.alert(newMessagesFrom);
		}
		if(instantMessageNotificationConf & 4)
		{
			let notification = new Notification('WoB Messenger', {	body: newMessagesFrom + " "});
		}
	}
    instantMessageNotificationBlock = 0;
    localStorage.setItem(characterName + 'instantMessageNotificationBlock', instantMessageNotificationBlock);
    instantMessageNotificationException = false;
}

function beep() {
    let beepSound = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    beepSound.play();
}